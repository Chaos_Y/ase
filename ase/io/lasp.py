"""
IO functions for lasp file formats.

The formats follow formatting, which similar to DMol3-arc file.

arc
----
line:   1-4   information lines
line:     5   heading by "PBC", followed by lattice "a b c alpha beta gamma"
line:   6--   Element name; Cartesian coordination x y z; "CORE"; Index; Element name; Element name; Index
line:   End   "End"   
line:   End   "End"
"""
import numpy as np
from ase import Atom, Atoms
from ase.geometry.cell import cell_to_cellpar, cellpar_to_cell
from ase.utils import reader, writer

@reader
def read_lasp_arc(fd, index=-1):
    """
    Read LASP arc file.
    """

    lines = fd.readlines()
    images = []
    
    if lines[1].startswith('PBC=ON'):
        pbc = True
    elif lines[1].startswith('PBC=OFF'):
        pbc = False
    else:
        raise RuntimeError('Could not read pbc from second line in file')

    i = 0
    while i < len(lines):
        cell = np.zeros((3, 3))
        symbols = []
        positions = []
        charges = []
        energy = []
        maxforce = []
        
        if 'Energy' in lines[i]:
            try:
                ienergy = float(lines[i].split()[-1])
                try:
                    imaxforce = float(lines[i].split()[-2])
                except Exception:
                    imaxforce = 0

                if ienergy.is_integer():
                    ienergy = float(lines[i].split()[-2])
                    imaxforce = 0
            except Exception:
                ienergy = float(lines[i].split()[-2])
                imaxforce = 0
            i += 1
        
        if lines[i].startswith('!DATE'):
            # read cell
            # NOTE: cell for arc file is "a b c alpha beta gamma", if should be transfered to 3X3 matrix.
            #       Besides, the direct transfer by cellpar_to_cell may lead atoms outside cell.
            if pbc:
                cell_dat = np.array([float(fld)
                                     for fld in lines[i + 1].split()[1:7]])
                cell = cellpar_to_cell(cell_dat)
                i += 1
            i += 1
            # read atoms
            while not lines[i].startswith('end'):
                flds = lines[i].split()
                symbols.append(flds[0])
                positions.append(flds[1:4])
                try:
                    charges.append(flds[8])
                except Exception:
                    charges.append(0.0000)
                i += 1
            image = Atoms(symbols=symbols, positions=positions, cell=cell, charges=charges,
                          pbc=pbc, info={'energy': ienergy, 'maxforce': imaxforce})
            images.append(image)
        if len(images) == index:
            return images[-1]
        i += 1

    # return requested images, code borrowed from ase/io/trajectory.py
    if isinstance(index, int):
        return images[index]
    else:
        from ase.io.formats import index2range
        indices = index2range(index, len(images))
        return [images[j] for j in indices]
    

@writer
def write_lasp_arc(fd, images):
    """
    Write the LASP arc file.
    """
    fd.write('!BIOSYM archive 2\n')
    if np.all(images[0].pbc):
        fd.write('PBC=ON\n')
        # Rotate positions so they will align with cellpar cell
    elif not np.any(images[0].pbc):
        fd.write('PBC=OFF\n')
    else:
        raise ValueError('PBC must be all true or all false for .arc format')
    
    for istr, atoms in enumerate(images):
        try:
            fd.write('                      Energy     %8d    %8d     %12.6f\n' %
                        (istr, istr, atoms.info['energy']))
        except KeyError:
            fd.write('                      Energy     %8d    %8d     %12.6f\n' %
                        (istr, istr, 0.0))
        symbols = atoms.get_chemical_symbols()
        charges = atoms.get_initial_charges()
        if np.all(atoms.pbc):
            cellpar = cell_to_cellpar(atoms.cell)
            fd.write('!DATE     \n')
            fd.write('PBC %9.5f %9.5f %9.5f %9.5f %9.5f %9.5f\n'
                     % tuple(cellpar))
            positions = atoms.positions
        elif not np.any(atoms.pbc):  # [False,False,False]
            fd.write('!DATE     \n')
            positions = atoms.positions
        else:
            raise ValueError(
                'PBC must be all true or all false for .arc format')
        
        # Sorted the structure accoring to symbol and positions
        data = sorted(zip(symbols, positions, charges), key=lambda x: (x[0], x[1][2]))

        for i, (sym, pos, charge) in enumerate(data):
            fd.write(
                '%-3s %15.9f %15.9f %15.9f CORE %4d %2s %2s %8.4f %4d\n'
                  % (sym, pos[0], pos[1], pos[2], i+1, sym, sym, charge, i+1))
        fd.write('end\nend\n')
        #fd.write('\n')

    
